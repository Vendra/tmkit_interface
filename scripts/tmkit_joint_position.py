#!/usr/bin/env python

import argparse
from _ast import operator
import rospy
import time
import baxter_interface
import baxter_external_devices
from geometry_msgs.msg import Pose,Point,Quaternion
from baxter_interface import CHECK_VERSION
import struct
import sys
import copy
import rospy
import rospkg

from gazebo_msgs.srv import (
    SpawnModel,
    DeleteModel,
)
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from matplotlib.backend_bases import PickEvent
from std_msgs.msg import (
    Header,
    Empty,
)

from baxter_core_msgs.srv import (
    SolvePositionIK,
    SolvePositionIKRequest,
)

class PickAndPlace(object):
    def __init__(self, limb, hover_distance=0.15, verbose=True):
        self._limb_name = limb  # string
        self._hover_distance = hover_distance  # in meters
        self._verbose = verbose  # bool
        self._limb = baxter_interface.Limb(limb)
        self._gripper = baxter_interface.Gripper(limb)
        ns = "ExternalTools/" + limb + "/PositionKinematicsNode/IKService"
        self._iksvc = rospy.ServiceProxy(ns, SolvePositionIK)
        rospy.wait_for_service(ns, 5.0)
        # verify robot is enabled
        print("Getting robot state... ")
        self._rs = baxter_interface.RobotEnable(baxter_interface.CHECK_VERSION)
        self._init_state = self._rs.state().enabled
        print("Enabling robot... ")
        self._rs.enable()

    def move_to_start(self, start_angles=None):
        print("Moving the {0} arm to start pose...".format(self._limb_name))
        if not start_angles:
            start_angles = dict(zip(self._joint_names, [0] * 7))
        self._guarded_move_to_joint_position(start_angles)
        self.gripper_open()
        rospy.sleep(1.0)
        print("Running. Ctrl-c to quit")

    def ik_request(self, pose):
        hdr = Header(stamp=rospy.Time.now(), frame_id='base')
        ikreq = SolvePositionIKRequest()
        ikreq.pose_stamp.append(PoseStamped(header=hdr, pose=pose))
        try:
            resp = self._iksvc(ikreq)
        except (rospy.ServiceException, rospy.ROSException) as e:
            rospy.logerr("Service call failed: %s" % (e,))
            return False

        # Check if result valid, and type of seed ultimately used to get solution
        # convert rospy's string representation of uint8[]'s to int's
        resp_seeds = struct.unpack('<%dB' % len(resp.result_type), resp.result_type)
        limb_joints = {}
        if (resp_seeds[0] != resp.RESULT_INVALID):
            seed_str = {
                ikreq.SEED_USER: 'User Provided Seed',
                ikreq.SEED_CURRENT: 'Current Joint Angles',
                ikreq.SEED_NS_MAP: 'Nullspace Setpoints',
            }.get(resp_seeds[0], 'None')
            if self._verbose:
                print("IK Solution SUCCESS - Valid Joint Solution Found from Seed Type: {0}".format(
                    (seed_str)))
            # Format solution into Limb API-compatible dictionary
            limb_joints = dict(zip(resp.joints[0].name, resp.joints[0].position))
            if self._verbose:
                print("IK Joint Solution:\n{0}".format(limb_joints))
                print("------------------")
        else:
            rospy.logerr("INVALID POSE - No Valid Joint Solution Found.")
            return False
        return limb_joints

    def _guarded_move_to_joint_position(self, joint_angles):
        if joint_angles:
            self._limb.move_to_joint_positions(joint_angles)
        else:
            rospy.logerr("No Joint Angles provided for move_to_joint_positions. Staying put.")

    def gripper_open(self):
        self._gripper.open()
        rospy.sleep(1.0)

    def gripper_close(self):
        self._gripper.close()
        rospy.sleep(1.0)

    def _approach(self, pose):
        approach = copy.deepcopy(pose)
        # approach with a pose the hover-distance above the requested pose
        approach.position.z = approach.position.z + self._hover_distance
        joint_angles = self.ik_request(approach)
        self._guarded_move_to_joint_position(joint_angles)

    def _retract(self):
        # retrieve current pose from endpoint
        current_pose = self._limb.endpoint_pose()
        ik_pose = Pose()
        ik_pose.position.x = current_pose['position'].x
        ik_pose.position.y = current_pose['position'].y
        ik_pose.position.z = current_pose['position'].z + self._hover_distance
        ik_pose.orientation.x = current_pose['orientation'].x
        ik_pose.orientation.y = current_pose['orientation'].y
        ik_pose.orientation.z = current_pose['orientation'].z
        ik_pose.orientation.w = current_pose['orientation'].w
        joint_angles = self.ik_request(ik_pose)
        # servo up from current pose
        self._guarded_move_to_joint_position(joint_angles)

    def _servo_to_pose(self, pose):
        # servo down to release
        joint_angles = self.ik_request(pose)
        self._guarded_move_to_joint_position(joint_angles)

    def pick(self, pose):
        # open the gripper
        self.gripper_open()
        # servo above pose
        self._approach(pose)
        # servo to pose
        self._servo_to_pose(pose)
        # close gripper
        self.gripper_close()
        # retract to clear object
        self._retract()

    def place(self, pose):
        # servo above pose
        self._approach(pose)
        # servo to pose
        self._servo_to_pose(pose)
        # open the gripper
        self.gripper_open()
        # retract to clear object
        self._retract()



def set_j(limb, joints_name, new_positions):
    joint_commands = {}
    for i in range(0, len(joints_name)):
        joint_commands[joints_name[i]] = new_positions[i]

    limb.set_joint_position_speed(speed=0.7)
    limb.move_to_joint_positions(joint_commands, timeout=25.0, threshold=0.00174533)

def parse_plan(path):
    left = baxter_interface.Limb('left')
    right = baxter_interface.Limb('right')
    grip_left = baxter_interface.Gripper('left', CHECK_VERSION)
    grip_right = baxter_interface.Gripper('right', CHECK_VERSION)
    lj = left.joint_names()
    rj = right.joint_names()

    #Test
    pnp = PickAndPlace('right')

    toPick = False
    toPlace = False

    for line in open(path):
        #Executes Pick
        if(toPick and line[0] == 'r'):
            toPick = False  #reset
            grip_right.open()  #Open the gripper
            #time.sleep(0.5)

            #get current Pose
            current_pose = right.endpoint_pose()
            ik_pose = Pose()
            ik_pose.position.x = current_pose['position'].x        #- 0.022
            ik_pose.position.y = current_pose['position'].y        #+ 0.017
            ik_pose.position.z = current_pose['position'].z - 0.2  # 0.23
            #Use the same orientation
            ik_pose.orientation.x = current_pose['orientation'].x
            ik_pose.orientation.y = current_pose['orientation'].y
            ik_pose.orientation.z = current_pose['orientation'].z
            ik_pose.orientation.w = current_pose['orientation'].w

            #Approach the object
            pnp._servo_to_pose(ik_pose)
            time.sleep(1.5)

            # Grasp the object
            grip_right.close()
            time.sleep(1.5)

            # Go back Up
            ik_pose.position.x = ik_pose.position.x #+ 0.022
            ik_pose.position.y = ik_pose.position.y #- 0.017
            ik_pose.position.z = ik_pose.position.z + 0.2
            pnp._servo_to_pose(ik_pose)
            time.sleep(0.5)

        #Executes Place
        if(toPlace and line[0] == 'r'):
            toPlace = False

            current_pose = right.endpoint_pose()
            ik_pose = Pose()
            ik_pose.position.x = current_pose['position'].x   # x>0 goes forward
            ik_pose.position.y = current_pose['position'].y
            ik_pose.position.z = current_pose['position'].z
            #Use the same orientation
            ik_pose.orientation.x = current_pose['orientation'].x
            ik_pose.orientation.y = current_pose['orientation'].y
            ik_pose.orientation.z = current_pose['orientation'].z
            ik_pose.orientation.w = current_pose['orientation'].w
            # Make it perpendicular - Maybe it works better this way
            #ik_pose.orientation.x = 0.707
            #ik_pose.orientation.y = 0.707
            #ik_pose.orientation.z = 0.0
            #ik_pose.orientation.w = 0.0
            print("BEFORE")
            print(current_pose)

            #pnp._servo_to_pose(ik_pose)
            ik_pose.position.z = current_pose['position'].z - 0.2  # 0.23
            pnp._servo_to_pose(ik_pose)
            time.sleep(0.5)

            current_pose = right.endpoint_pose()
            print("AFTER")
            print(current_pose)

            #release
            grip_right.open()
            time.sleep(2)

            #get back to original position
            ik_pose.position.z = current_pose['position'].z + 0.2
            pnp._servo_to_pose(ik_pose)

        if (line[0] == 'a'):
            line = line[1:]
            operation_line = line.split()
            print(operation_line[0])
            if (operation_line[0] == 'UNSTACK' or operation_line[0] == 'PICK-UP'):
                toPick = True
            if (operation_line[0] == 'STACK' or operation_line[0] == 'PUT-DOWN'):
                toPlace = True

        if (line[0] == 'm'):
            line = line[1:]
            joint_names = line.split()
            print(joint_names)

        if (line[0] == 'p'):
            line = line[1:]
            joint_positions = line.split()
            joint_positions[:] = [float(x) for x in joint_positions]

            if (len(joint_names) != len(joint_positions)):
                print("joint names and positions are not the same size!")
                exit(-1)

            left_joints_names = []
            left_joints_positions = []
            right_joints_names = []
            right_joints_positions = []

            for i in range(0, len(joint_positions)):
                if ("left" in joint_names[i]):
                    left_joints_names.append(joint_names[i])
                    left_joints_positions.append(joint_positions[i])

                if ("right" in joint_names[i]):
                    right_joints_names.append(joint_names[i])
                    right_joints_positions.append(joint_positions[i])

            #This way I can command joints belonging to the same limb simultaneously
            if(len(left_joints_names) != 0):
                set_j(left, left_joints_names, left_joints_positions)
            if(len(right_joints_names) != 0):
                set_j(right, right_joints_names, right_joints_positions)


def main(path):
    print("Initializing node... ")
    rospy.init_node("baxter_joint_position_tmkit")
    print("Getting robot state... ")
    rs = baxter_interface.RobotEnable(CHECK_VERSION)
    init_state = rs.state().enabled

    def clean_shutdown():
        print("\nExiting example...")
        #Commented otherwise the robot starts to "float"
        #if not init_state:
            #print("Disabling robot...")
            #rs.disable()

    rospy.on_shutdown(clean_shutdown)

    print("Enabling robot... ")
    rs.enable()

    parse_plan(path)
    print("Done.")


if __name__ == '__main__':
    main(sys.argv[1])
