Federico Vendramin 31st January 2018
# tmkit_interface

A TMKIT interface for ROS and Baxter SDK in Gazebo, requires a valid output tmkit plan.

===========================
==         USAGE         ==
===========================

#Load the environment:
$ ./baxters.sh sim
$ roslaunch tmkit_interface baxter_world.launch

#Place the robot in a safe position to avoid collision during the spawning of the scene, type in another terminal:
$ rosrun tmkit_interface tmkit_joint_position.py tmkit_interface/plans/q0.tmp

#Then spawn the scene:
$ rosrun gazebo_ros spawn_model -file tmkit_interface/models/reconstructed_table.urdf -urdf -model table
$ roslaunch tmkit_interface spawn_scene

#Start the plan execution:
$ rosrun tmkit_interface tmkit_joint_position tmkit_interface/plans/pick_and_place_plan.tmp








